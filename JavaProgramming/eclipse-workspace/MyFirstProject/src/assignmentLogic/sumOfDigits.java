package assignmentLogic;
import java.util.Scanner;

public class sumOfDigits {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		long num, i;		
		System.out.print("Enter a number: ");
		num=sc.nextLong();
		
		for(i=0; num!=0; num=num/10) {
			i = i + num % 10;  
		}
		System.out.println("Sum of digits: "+i);
		sc.close();
		sc.close();
	}

}
