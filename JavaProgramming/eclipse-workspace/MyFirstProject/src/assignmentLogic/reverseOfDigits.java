package assignmentLogic;
import java.util.Scanner;

public class reverseOfDigits {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number to be reversed: ");
		long num = sc.nextInt();
		long reverse=0;
		long reminder=0;
		
		for (;num!=0;num=num/10) {
			reminder = num%10;
			reverse = reverse*10 + reminder;
		}
		System.out.println("The reverse of the number is: "+reverse);
		sc.close();

	}

}
