package assignmentLogic;
import java.util.Scanner;

public class validateEvenOdd {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the value for the first integer: ");
		int a = sc.nextInt();
		
		try {			
			if (a%2 == 0) {
				System.out.println("The given integer "+a+" is Even number");
			}else {
				System.out.println("The given integer "+a+" is Odd number");
			}
		}
		catch (Exception e) {
			System.out.println("Enter valid integers");
		}
		sc.close();

	}

}
