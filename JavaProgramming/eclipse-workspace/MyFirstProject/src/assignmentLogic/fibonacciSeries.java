package assignmentLogic;
import java.util.Scanner;

public class fibonacciSeries {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n1=0, n2=1,n3=0;
		System.out.println("Enter the number to generate the fibonacci series: ");
		int counter = sc.nextInt();
		
		System.out.println(n1+" "+n2); //printing 0 and 1
		for(int i=2;i<=counter;++i) {
			n3 = n1 + n2;
			System.out.println(" "+n3);
			n1=n2;
			n2=n3;	
			
		}
		sc.close();

	}

}
