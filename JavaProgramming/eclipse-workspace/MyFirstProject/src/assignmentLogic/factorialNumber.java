package assignmentLogic;
import java.util.Scanner;

public class factorialNumber {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number to calculate the factorial");
		int num = sc.nextInt();
		int fact=1;
		
		for(int i=1;i<=num;i++) {
			fact = fact * i;
		}
		System.out.println("The Factorial of "+num+" is: "+fact);
		sc.close();
	}

}
