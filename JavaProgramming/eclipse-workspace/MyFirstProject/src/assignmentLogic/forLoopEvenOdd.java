package assignmentLogic;

public class forLoopEvenOdd {

	public static void main(String[] args) {
		
		String even = "10 even numbers: ";
		String odd = "10 odd numbers: ";
		int evenCounter = 0;
		int oddCounter = 0;
		for (int i=1; i<=20; i++) {			
			
			if (i%2 == 0) {
				if (evenCounter == 0) {
					evenCounter++;
					even = even + i;
				}else {
					even = even +", "+ i;
				}
				
			}else {
				if (oddCounter == 0) {
					oddCounter++;
					odd = odd + i;
				}else {
					odd = odd +", "+ i;
				}
				
			}
		}
		System.out.println(even+"\n"+odd);

	}

}
