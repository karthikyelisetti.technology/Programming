package assignmentLogic;

import java.util.Scanner;

public class arithmeticOperators {

	public static void main(String[] args) {
		int a = 0, b = 0, c = 0;
		boolean bool;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the value for logical operation: A:+, S:-, M:*, D:/, I:++, R:");
		char value = sc.next().charAt(0);
		if (value == 'A' || value == 'S' || value == 'M' || value == 'D'
				|| value == 'I' || value == 'R') {
			bool=true;
			System.out.println("Enter the value for a: ");
			a=sc.nextInt();
			System.out.println("Enter the value for b: ");
			b=sc.nextInt();
		}else {
			bool=false;
			System.out.println("Please enter a valid option");
		}
		
		if (value == 'A') {
			c=a+b;
		}else if (value == 'S') {
			c=a-b;
		}else if (value == 'M') {
			c=a*b;
		}else if (value == 'D') {
			c=a/b;
		}else if (value == 'I') {
			c=a+b;
			c++;
		}else if (value == 'R') {
			c=a-b;
			c--;
		}
		sc.close();
		if (bool)
			System.out.println("The result is = "+c);

	}

}
