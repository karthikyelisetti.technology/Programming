package assignmentLogic;
import java.util.Scanner;

public class votingEligibility {

	public static void main(String[] args) {
		int age;		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the Age of the candidate: ");
		age = sc.nextInt();
		
		try {
			if (age >= 18) {
				System.out.println("Candidate is Eligible for voting");
			}else if (age > 0 && age < 18){
				System.out.println("Candidate is not Eligible for voting");
			}else {
				System.out.println("Age value needs to be always a positive number");
			}
		}catch(Exception e) {
			System.out.println("Enter valid Age");
		}
		sc.close();
	}

}
