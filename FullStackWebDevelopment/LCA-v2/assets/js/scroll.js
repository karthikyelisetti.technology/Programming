
function scrollNav() {
  $('.main-nav a').click(function(){
    $(".active").removeClass("active");     
    $(this).addClass("active");
    
    $('html, body').stop().animate({
      scrollTop: $($(this).attr('href')).offset().top - 20
    }, 1000);
    return false;
  });
}
scrollNav();